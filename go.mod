module modernc.org/scannertest

go 1.16

require (
	modernc.org/lex v1.1.1
	modernc.org/lexer v1.0.4
)
